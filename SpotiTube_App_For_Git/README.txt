IMPORTANT: 
DO NOT MOVE AROUND ANY FILES OR THE Spotitube.bat FILE AS IT WILL CAUSE THE APP TO GET CORRUPTED, thank you for understanding :)
---------------------------------------------------------------------------------------------------------------------------
TIPS:
If the app does not function as intended, please reinstall the app

If you want to launch the app from a location such as your Desktop, simply go to the Spotitube.bat file and make a shortcut and put the shortcut to your specified location (such as your Desktop).

Instructions on deleting saved YouTube songs/videos from the app are outlined in the More Information button located on the top menu of the app

OTHER:
This app is NOT meant for commercial use and was made for the Congressional App Challenge 2019