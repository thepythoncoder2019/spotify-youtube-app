import requests as r
import urllib.parse


class YouTube_Title:
    def __init__(self):
        self.api_key = ''
        self.endpoint = 'https://www.googleapis.com/youtube/v3/videos'

    def get_title(self, url:list):
        id_list = []
        self.titles = []
        for a in range(len(url)):
            video_id = urllib.parse.urlparse(url[a])
            video_id = urllib.parse.parse_qs(video_id.query)['v'][0]
            id_list.append(video_id)

        ids_string = ','.join(id_list)
        params = {'part': 'snippet', 'id': ids_string, 'key': os.environ['API_KEY']}
        title = r.get(self.endpoint, params)
        for a in range(len(title.json()['items'])):
            self.titles.append(title.json()['items'][a]['snippet']['title'])


