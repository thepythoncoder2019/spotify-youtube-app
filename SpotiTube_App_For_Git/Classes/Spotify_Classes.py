import requests as r
import urllib.parse
import base64
import os

class ConnectSpotify:
    def __init__(self):
        self.client_id = ''
        self.client_secret = ''
        self.authorize_endpoint = 'https://accounts.spotify.com/authorize'
        self.authorize_params = {'scope': 'user-library-read,playlist-read-private',
                                 'redirect_uri': 'https://www.google.com/', 'response_type': 'code',
                                 'client_id': os.environ['CLIENT_ID']}

    def authorize_request(self):
        authorize_request = r.get(self.authorize_endpoint, params=self.authorize_params)
        print('Instructions on connecting your Spotify account and selected playlist with SpotiTube:')
        print('Step 1: Please click on the link below to authorize your account, once you have authorized it you will be redirected to a website.\nNOTE: DO NOT CLOSE THE WEBSITE YOU WERE REDIRECTED TO AS IT CONTAINS YOUR AUTHORIZATION CODE!')
        print(authorize_request.url)
        self.authcode = input('Step 2. Please enter the link of the website you were redirected to: ')
        self.authcode = urllib.parse.urlparse(self.authcode)
        self.authcode = urllib.parse.parse_qs(self.authcode.query)['code'][0]

    def token_request(self):
        token_url = 'https://accounts.spotify.com/api/token'
        redirect_uri = 'https://www.google.com/'
        self.token_request = r.post(token_url, data={'grant_type': 'authorization_code', 'code': self.authcode,
                                                     'redirect_uri': redirect_uri, 'client_id': os.environ['CLIENT_ID'],
                                                     'client_secret': os.environ['CLIENT_SECRET']},
                                    headers={'Content-Type': 'application/x-www-form-urlencoded'})

    def check_request(self):
        if self.token_request.status_code == 200:
            print('Connection to account successful!')
            open_r_token = open('refresh_token.txt', 'w')
            open_r_token.write(self.token_request.json()['refresh_token'])
            open_r_token.close()
            return True

        else:
            print('Connection unsuccessful! Please close and reopen the app!')
            return False

    def get_playlist_id(self):
        endpoint = 'https://api.spotify.com/v1/me/playlists'
        open_token = open('refresh_token.txt', 'r')
        refresh_token = open_token.read()
        open_token.close()
        client_id = ''
        client_secret = ''
        client64 = base64.b64encode(os.environ['CLIENT_ID'].encode() + ':'.encode() + os.environ['CLIENT_SECRET'].encode())
        refresh_request = r.post('https://accounts.spotify.com/api/token',
                                      headers={'Content-Type': 'application/x-www-form-urlencoded',
                                               'Authorization': 'Basic ' + client64.decode()},
                                      data={'grant_type': 'refresh_token', 'refresh_token': refresh_token})
        a_token = refresh_request.json()['access_token']

        if 'refresh_token' in refresh_request.json():
            open_refresh_token = open('refresh_token.txt', 'w')
            open_refresh_token.seek(0)
            open_refresh_token.write(refresh_request.json()['refresh_token'])
            open_refresh_token.close()

        else:
            pass

        request_playlists = r.get(endpoint, headers = {'Authorization':'Bearer ' + a_token})
        for a in range(len(request_playlists.json()['items'])):
            print(str(a) + '.', request_playlists.json()['items'][a]['name'])

        playlist_num_to_connect = input('Step 3: Please type the menu number of the playlist you would like to connect: ')
        playlist_num_to_connect = int(playlist_num_to_connect)

        playlist_id = request_playlists.json()['items'][playlist_num_to_connect]['tracks']['href']
        open_url = open('playlisttracks.txt', 'w')
        open_url.write(playlist_id)
        open_url.close()
        self.playlistconnect = True
        if self.playlistconnect is True:
            print('Connection to playlist successful! To start using SpotiTube, simple close the app then reopen it.')
        else:
            self.playlistconnect = False
            print('Connection to playlist unsuccessful! Please try again by closing then opening the app again.')

class GetSpotifyTracks:
    def __init__(self):
        open_token = open('refresh_token.txt', 'r')
        self.refresh_token = open_token.read()
        open_token.close()

    def get_tokens(self):
        client_id = ''
        client_secret = ''
        client64 = base64.b64encode(os.environ['CLIENT_ID'].encode() + ':'.encode() + os.environ['CLIENT_SECRET'].encode())
        self.refresh_request = r.post('https://accounts.spotify.com/api/token',
                                 headers={'Content-Type': 'application/x-www-form-urlencoded',
                                          'Authorization': 'Basic ' + client64.decode()},
                                 data={'grant_type': 'refresh_token', 'refresh_token': self.refresh_token})
        self.a_token = self.refresh_request.json()['access_token']

        if 'refresh_token' in self.refresh_request.json():
            open_refresh_token = open('refresh_token.txt', 'w')
            open_refresh_token.seek(0)
            open_refresh_token.write(self.refresh_request.json()['refresh_token'])
            open_refresh_token.close()

        else:
            pass

    def get_tracks(self):
        names = []
        urls = []
        playlist_url = open('playlisttracks.txt', 'r')
        playlist_url_get_tracks = playlist_url.read()
        playlist_url.close()
        user_tracks_endpoint = playlist_url_get_tracks
        library_headers = {'Authorization': 'Bearer ' + self.a_token}
        tracks = r.get(user_tracks_endpoint, headers=library_headers)

        for a in range(len(tracks.json()['items'])):
            name = tracks.json()['items'][a]['track']['name']
            url = tracks.json()['items'][a]['track']['external_urls']['spotify']
            names.append(name)
            urls.append(url)

        self.names = names
        self.urls = urls
