import webbrowser
import tkinter
from tkinter import messagebox
import sys
import random
from Classes.Spotify_Classes import ConnectSpotify
from Classes.Spotify_Classes import GetSpotifyTracks
from Classes.YouTube_Class import YouTube_Title
import csv

class Video:
    def __init__(self):
        gui.destroy()
        self.links = []
        self.video_window = tkinter.Tk()
        self.video_window.config(bg = 'red')
        self.video_window.iconbitmap('icon.ico')

    def create_objects(self):
        self.link_entry = tkinter.Entry(width = 50)
        self.link_entry.insert(0, 'Enter the YouTube video link here')
        self.add_to_list_button = tkinter.Button(text='Add to List of Links to be Saved', command=self.save_to_list)
        self.submit_button = tkinter.Button(
            text='Submit All Links to be Saved (Press the button once you have added all of your links)',
            command=self.submit_button_click)
        self.quit_button = tkinter.Button(text = 'Quit', command = self.quit_function, bg = 'green')
        self.credits_label = tkinter.Label(text = 'App coded by Paras', bg = 'red')
        self.toplabel = tkinter.Label(text = 'To add a YouTube video to your songs, simply type the url to that video in the box below and click the Add to List of Links to be Saved button.', bg = 'red')
        self.toplabel2 = tkinter.Label(text = 'By using this button, you are adding your video to the list of videos to be saved which allows you to save multiple videos. Once you are done,', bg = 'red')
        self.toplabel3 = tkinter.Label(text = 'adding all of your videos, simply click the Submit all links to be saved button.', bg = 'red')
        self.empty_label = tkinter.Label(text = '', bg = 'red')

    def save_to_list(self):
        link_to_be_saved = self.link_entry.get()
        self.links.append(link_to_be_saved)
        self.link_entry.delete(0, 'end')
        self.link_entry.insert(0, 'Link added, type in the next link or select submit if you are done')

    def submit_button_click(self):
        self.list_of_urls = self.links
        self.video_window.destroy()

    def pack_all(self):
        self.toplabel.pack()
        self.toplabel2.pack()
        self.toplabel3.pack()
        self.empty_label.pack()
        self.link_entry.pack()
        self.add_to_list_button.pack()
        self.submit_button.pack()
        self.quit_button.pack(side = 'bottom')
        self.credits_label.pack(side = 'bottom')

    def quit_function(self):
        sys.exit()

def check_if_connected_before():
    file = open('connected.txt', 'r')
    read_file = file.read()

    if read_file == 'Yes':
        return True

    elif read_file == 'No':
        return False

    file.close()

def add_video():
    v = Video()
    v.create_objects()
    v.pack_all()
    v.video_window.state('zoomed')
    v.video_window.title('SpotiTube')
    v.video_window.mainloop()
    video_urls_list = v.list_of_urls
    t = YouTube_Title()
    t.get_title(video_urls_list)
    titles_list = t.titles
    c = open('titlelink.csv', 'a')
    write = csv.writer(c)
    for a in range(len(titles_list)):
        list = []
        list.append(titles_list[a])
        list.append(video_urls_list[a])
        write.writerow(list)
    c.close()
    messagebox.showinfo('Information', 'The songs have been saved! Please close then open the app!')
    sys.exit()

connected = check_if_connected_before()

def quit_function():
    sys.exit()

def more_info():
    gui.destroy()
    moreinfo = tkinter.Tk()
    moreinfo.state('zoomed')
    moreinfo.config(bg = 'orange')
    appcontestlabel = tkinter.Label(text = 'This app was created for the Congressional App Challenge! :)', bg = 'orange')
    spacer2 = tkinter.Label(text = '', bg = 'orange')
    about = tkinter.Label(text = 'What is SpotiTube?', bg = 'orange')
    about2 = tkinter.Label(text = 'SpotiTube is an app designed to help you get your music in order so you can spend less', bg = 'orange')
    about3 = tkinter.Label(text = 'time looking for your music and more time working on items which are more important.', bg = 'orange')
    about4 = tkinter.Label(text = 'SpotiTube lets you connect your favorite Spotify playlist and add your favorite songs', bg = 'orange')
    about5 = tkinter.Label(text = 'from YouTube so you can access all of it from the app. When you click on the song you want', bg = 'orange')
    about6 = tkinter.Label(text = 'to play, you will be redirected to the corresponding website where the song is stored.', bg = 'orange')
    spacer5 = tkinter.Label(text = '', bg = 'orange')
    label1 = tkinter.Label(text = 'A normal display can typically display up to 25 combined songs', bg = 'orange')
    spacer = tkinter.Label(text = '', bg = 'orange')
    label2 = tkinter.Label(text = 'Instructions on removing a YouTube video that you have added:', bg = 'orange')
    label3 = tkinter.Label(text = 'Step 1: Open the app directory and select the titlelink.txt file', bg = 'orange')
    label4 = tkinter.Label(text = 'Step 2: Once you have opened the file, navigate to the row of the song', bg = 'orange')
    label5 = tkinter.Label(text = 'you would like to delete and delete the whole row as well as the empty row below. After that, simply save and close the file!', bg = 'orange')
    spacer3 = tkinter.Label(text = '', bg = 'orange')
    label6 = tkinter.Label(text = 'Spotify is a trademark of Spotify AB and YouTube is a trademark of Google LLC. This app has not been', bg = 'orange')
    label7 = tkinter.Label(text = 'endorsed by either companies and is not affiliated with either companies.', bg = 'orange')
    spacer4 = tkinter.Label(text = '', bg = 'orange')
    maker_credits = tkinter.Label(text = 'SpotiTube was coded by Paras :)', bg = 'orange')
    quit_button2 = tkinter.Button(text = 'Quit', command = quit_function, bg = 'yellow')
    appcontestlabel.pack()
    spacer2.pack()
    about.pack()
    about2.pack()
    about3.pack()
    about4.pack()
    about5.pack()
    about6.pack()
    spacer5.pack()
    spacer2.pack()
    label1.pack()
    spacer.pack()
    label2.pack()
    label3.pack()
    label4.pack()
    label5.pack()
    spacer3.pack()
    label6.pack()
    label7.pack()
    spacer4.pack()
    image = tkinter.PhotoImage(file = 'appchallenge.gif')
    image_label = tkinter.Label(image = image)
    image_label.pack(side = 'bottom')
    quit_button2.pack(side = 'bottom')
    maker_credits.pack(side = 'bottom')
    moreinfo.title('SpotiTube')
    moreinfo.iconbitmap('icon.ico')
    moreinfo.mainloop()

if connected is True:
    spot = GetSpotifyTracks()
    spot.get_tokens()
    spot.get_tracks()
    track_names = spot.names
    track_urls = spot.urls

    gui = tkinter.Tk()

    label = tkinter.Label(text = 'Welcome to SpotiTube! Your Spotify and YouTube songs are listed below, to add a YouTube song simply click on the button in the top left corner!', master = gui)
    label.config(bg = 'white')
    empty_label = tkinter.Label(text = '', master = gui)
    empty_label.config(bg = 'green')
    label.pack()
    empty_label.pack()
    menu = tkinter.Menu(master = gui)
    menu.add_command(label = 'Add YouTube video', command = add_video)
    menu.add_command(label = 'More Information', command = more_info)
    gui.config(menu = menu)


    for a in range(len(track_names)):
        button = tkinter.Button(master = gui, text = track_names[a], command = lambda url_to_open = track_urls[a]: webbrowser.open(url_to_open))
        random_int = random.randint(0, 4)
        if random_int == 0:
            button.config(bg = 'red')
        elif random_int == 1:
            button.config(bg = 'orange')
        elif random_int == 2:
            button.config(bg = 'yellow')
        elif random_int == 3:
            button.config(bg = 'blue')
        elif random_int == 4:
            button.config(bg = 'green')
        button.pack()

    c = open('titlelink.csv', 'r')
    r = csv.reader(c)
    youtube_list = list(r)

    if len(youtube_list) == 0:
        pass

    else:
        lists = youtube_list
        for l in range(len(lists)):
            current_list = lists[l]
            if len(current_list) == 0:
                pass
            else:
                title = current_list[0]
                link = current_list[1]
                youtube_button = tkinter.Button(text = title, command = lambda website = link: webbrowser.open(website), master = gui)
                random_int = random.randint(0, 4)
                if random_int == 0:
                    youtube_button.config(bg = 'red')
                elif random_int == 1:
                    youtube_button.config(bg = 'orange')
                elif random_int == 2:
                    youtube_button.config(bg = 'yellow')
                elif random_int == 3:
                    youtube_button.config(bg = 'blue')
                elif random_int == 4:
                    youtube_button.config(bg = 'green')
                youtube_button.pack()

    gui.title('SpotiTube')
    gui.state('zoomed')
    quit_button = tkinter.Button(text = 'Quit', command = quit_function, bg = 'red', master = gui)
    quit_button.pack(side = 'bottom')
    company_credits = tkinter.Label(text = 'Spotify is a trademark of Spotify AB and YouTube is a trademark of', bg = 'green', master = gui)
    company_credits2 = tkinter.Label(text = 'Google LLC. In no way is this app endorsed by either companies.', bg = 'green', master = gui)
    company_credits2.pack(side = 'bottom')
    company_credits.pack(side = 'bottom')
    app_credits = tkinter.Label(text = 'App coded by Paras', master = gui)
    app_credits.config(bg = 'green')
    app_credits.pack(side = 'bottom')
    gui.config(bg = 'green')
    gui.iconbitmap('icon.ico')
    gui.mainloop()

elif connected is False:
    connect = ConnectSpotify()
    connect.authorize_request()
    connect.token_request()
    check = connect.check_request()
    connect.get_playlist_id()

    if check is True and connect.playlistconnect is True:
        change = open('connected.txt', 'w')
        change.seek(0)
        change.write('Yes')
        change.close()

    elif check is False:
        pass

    elif connect.playlistconnect is False:
        pass